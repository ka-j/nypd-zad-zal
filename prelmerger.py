import argparse
import os
from typing import Type
from prel.data_handlers import tom_dh, stroop_dh
from prel.data_handlers.data_handler import DataHandler
from prel.data_handlers.prel_dh import PrelDataHandler


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description='Ten skrypt wczytuje folder z danymi, czysci je i liczy statystyki. Zwraca output w postaci pliku prelmerged.csv zapisanego w podanym folderze.')
    parser.add_argument('data_dir', help="scieżka do folderu z danymi. W srodku powinien być folder o nazwie 'raw' i podfoldery 'tom' i 'stroop',")
    parser.add_argument('-v', help='ustaw te flage aby wypisywac logi.', action='store_true')
    args = parser.parse_args()

    datadir = os.path.abspath(args.data_dir)
    rawdir = os.path.join(datadir, 'raw')
    cleandir = os.path.join(datadir, 'clean')
    statsdir = os.path.join(datadir, 'stats')
    merged_dir = os.path.join(datadir, 'merged')
    tomdir = '/tom/'
    stroopdir = '/stroop/'

    tomhandler = tom_dh.TomDataHandler(print_logs=args.v)
    stroophandler = stroop_dh.StroopDataHandler(print_logs=args.v)

    # czyszczenie:
    tomhandler.handle_files(rawdir+tomdir, cleandir +tomdir, tom_dh.clean_tom_data)
    stroophandler.handle_files(
        rawdir+stroopdir, cleandir+stroopdir, stroop_dh.clean_stroop_data)

    # liczymy statystyki
    tomhandler.handle_files(cleandir+tomdir, statsdir+tomdir,
                            lambda x: tom_dh.summarize_tom(tom_dh.count_tom_stats(x)))
    stroophandler.handle_files(cleandir+stroopdir, statsdir+stroopdir,
                               lambda x: stroop_dh.summarize_stroop(stroop_dh.count_stroop_stats(x)))

    # łączenie danych:
    prelhandler = PrelDataHandler(print_logs=args.v)
    prelhandler.merge_prel_files(statsdir+tomdir, statsdir+stroopdir, merged_dir)
    prelhandler.concat_files(merged_dir, 'prel_merged.csv')


def guess_handler_from_file(filename: str) -> Type[DataHandler]:
    handler = None
    if 'stroop' in filename.lower():
        handler = stroop_dh.StroopDataHandler
    elif 'tom' in filename.lower():
        handler = tom_dh.TomDataHandler
    else:
        # ??
        handler = DataHandler
    print(f"guessed: {handler.__name__} from filename: {filename}")
    return handler
