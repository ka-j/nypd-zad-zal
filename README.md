# Prel

Jest to biblioteka do czytania, czyszczenia i łączenia danych z eksperymentów:
- test Stroopa
- test teorii umysłu (ToM)

Korzystać z niej można za pomocą terminala lub jako pakiet pythonowy.

### Struktura

biblioteka składa się z obiektów klasy `DataHandler`, której zadaniem jest obsługa danych eskperymentalnych.

W sub-paczce prel.data_handlers znajdują się moduły data_handler.py, stroop_dh.py, tom_dh.py, prel_dh.py, a w nich:

- bazowa klasa `DataHandler`, odpowiedzialna za podstawowe funkcje input i output. Posiada metody czytania i zapisywania do plików, czytania folderów z danymi.
- klasa `PrelDataHandler` odpowiedzialna za pracę z plikami i folderami z danymi eksperymentalnych. Weryfikuje pliki i łączy z sobą dane. Wymaga id w nazwach plików (patrz sekcja Dane).  
- klasy `StroopDataHandler` i `TomDataHandler`, implementujące `PrelDataHandler` szczegółowe metody służące do czyszczenia i liczenia statystyk dla konkretnych eksperymentów - to operacje na obiektach DataFrame z biblioteki pandas.


## Dane

Biblioteka zakłada, że nazwy plików, na których pracuje zawierają przynajmniej jedną cyfrę. Za id uznaje się pierwszy ciąg cyfr w nazwie pliku. Pliki powinny być w formacie CSV, ale mogą mieć inne rozszerzenie.

### Struktura folderów
W archiwum data.zip znajdują się surowe pliki z obu eksperymentów, podzielone na podfoldery w proponowanej strukturze: w folderze 'data' znajduje się folder 'raw', w nim podfoldery dla obu eksperymentów z surowymi danymi eksperymentalnymi. Można tam znaleźć więcej niż jeden plik dla niektórych osób - to dlatego, że program z eksperymentem mógł być uruchamiany kilka razy (np. kiedy osoba badana chciała przejść ponownie etap treningowy zadania). Dane z obu eksperymentów niekoniecznie się pokrywają (możliwe, że dla niektórych osób badanych brakuje plików z któregoś eksperymentu). 

## Obsługa błędów

Program wypisuje odpowiednie ostrzeżenia, jeśli odczyta nieoczekiwane dane, albo znajdzie więcej niż jeden plik na osobę badaną. Funkcja `handle_files` informuje o błędach napotkanych w trakcie pracy nad poszczególnymi plikami, następnie pomija je. 

Funkcje czyszczące dane zwrócą puste obiekty DataFrame jeśli okaże się, że po czyszczeniu nie pozostały interesujące dane. Wtedy nie zapisują się do pliku, co pozwala uniknąć tworzenia kilku plików z danymi dla jednej osoby badanej. 

Funkcje liczące statystyki starają się poradzić sobie z danymi, które przeczytają, ale wyrzucą błędy (najprawdopodobniej KeyError lub ValueError), jeśli dane nie będą w oczekiwanym formacie.

W trakcie łączenia danych program będzie weryfikować, które pliki połączyć na podstawie id odczytanego z nazwy pliku, dlatego pliki bez cyfr w nazwie są wtedy pomijane. Sprawdzana jest też długość danych - spodziewamy się jednego wiersza w każdym pliku, ale program połączy też pliki o innych liczbach wierszy, tak jak robi to biblioteka pandas.

_Uwaga_: w przypadku pokrywających się nazw plików, biblioteka zawsze nadpisuje, ale za każdym razem o tym ostrzega.
_____

## Przykłady użycia  

W repozytorium jest plik example.ipynb, w którym są przykłady zastosowania biblioteki jako pakietu pythonowego.

w repozytorium znajduje się prosty skrypt prelmerger.py, który ilustruje wykorzystanie tej biblioteki. Jako argument najlepiej podać po prostu wypakowany z archiwum folder data. Opcjonalny argument -v powoduje, że do standardowego wyjścia wypisują się logi.

moduły: stroop_dh, tom_dh, prel_dh można uruchamiać z linii komend. Wtedy domyślnie pracują na pojedynczych plikach lub folderach zawierających pliki.
Przykładowe użycie:

` python -m prel.data_handlers.stroop_dh data/stroop/raw/ data/stroop/clean`

Moduł stroop_dh użyty domyślnie wyczyści pliki z pierwszego folderu podanego na wejściu i czyste dane zapisze w drugim, w plikach o takich samych nazwach co oryginalne.

_przykładowy output:_

`   21:33:39|prel_dh.verify_input_paths:119|WARNING: znalazłem więcej niż jeden plik dla id=02. |
    21:33:39|prel_dh.verify_input_paths:119|WARNING: znalazłem więcej niż jeden plik dla id=16. |
    21:33:39|prel_dh.verify_input_paths:119|WARNING: znalazłem więcej niż jeden plik dla id=26. |`

Uruchamiając w taki sam sposób moduł tom_dh możemy korzystać z analogicznych operacji dla danych z drugiego eksperymentu. Argumentami mogą być zarówno foldery jak pojedyncze pliki. Użycie flagi -v sprawi, że program wypisuje więcej informacji do standardowego wyjścia:

` python -m prel.data_handlers.tom_dh data/tom/raw/JSPREL_10_preludiumTom_2021_Nov_09_1730.csv data/tom/clean -v`

_output:_

`
21:41:43|data_handler.verify_input_paths:109|INFO: weryfikuję ścieżkę: data/tom/raw/JSPREL_10_preludiumTom_2021_Nov_09_1730.csv, file_extension=.csv |
21:41:43|data_handler.verify_output:138|INFO: w output_dir jest już 26 plików. |
21:41:43|data_handler.save_file:91|INFO: w folderze clean był już plik o tej samej nazwie: JSPREL_10_preludiumTom_2021_Nov_09_1730.csv i zostanie nadpisany. |`
`21:41:43|data_handler.handle_files:81|INFO:  Stworzyłem 1 plików w ...\data\tom\clean. |`

Poza czyszczeniem surowych danych, dostępne są metody pozwalające na liczenie statystyk. Za pomocą opcjonalnego trzeciego argumentu `action` wybieramy jaką operację chcemy wykonać na danych. Opcja 'stats' grupuje dane i liczy statystyki dla każdej podgrupy, poniżej przykładowe użycie:

`python -m prel.data_handlers.tom_dh data/tom/clean/ data/tom/temp stats -v
`

Opcja 'summ' jako wyjście daje tablicę z jednym wierszem (plik .csv z dwoma linijkami) z wyliczonymi statystykami dla pojedynczej osoby: średnia szybkość reakcji, procent poprawnych odpowiedzi. Dla danych z testu Stroopa liczy się tu wartość efektu Stroopa.

`python -m prel.data_handlers.tom_dh data/tom/temp/ data/tom/summaries summ 
`

Można poprzez terminal również łączyć (merge) i konkatenować pliki. Do tego celu używamy modułu prel_dh. Na przykład, aby połączyć dane z różnych eksperymentów, znajdujących się w folderach /stats/tom i /stats/stroop i wyniki zapisać w folderze /merged:

`python -m prel.data_handlers.prel_dh data/stats/tom data/stats/stroop data/merged merge`

Istotna jest kolejność - zawsze najpierw podajemy jako argument dane z ToM.

Aby skonkatenować dane z plików znajdujących się w pewnym folderze, jako pierwszy argument podajemy ten folder, jako drugi nazwę dla pliku wyjściowego, a jako argument `action` wpisujemy 'concat':

`python -m prel.data_handlers.prel_dh data/merged long_file.csv concat -v`