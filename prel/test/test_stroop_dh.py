import os

import pandas as pd
import pytest

from prel.data_handlers import stroop_dh as sdh


@pytest.fixture()
def handler(): yield sdh.StroopDataHandler(print_logs=True)


@pytest.fixture()
def raw_data() -> pd.DataFrame:
    yield pd.read_csv(os.path.dirname(__file__) + '/raw_stroop_data.txt', sep='\t')


@pytest.fixture()
def raw_training_data() -> pd.DataFrame:
    yield pd.read_csv(os.path.dirname(__file__) + '/raw_stroop_training_data.txt', sep='\t')


@pytest.fixture()
def other_data() -> pd.DataFrame:
    yield pd.DataFrame({'a': [1, None, 3], 'b': ['NA', 5, 6]})


def test_clean_training(raw_training_data):
    assert len(sdh.clean_stroop_data(raw_training_data)) == 0  # dane z treningu powinny byc odrzucone


def test_clean_stroop(raw_data):
    clean_data = sdh.clean_stroop_data(raw_data)
    assert len(clean_data) > 0
    assert "Task", "congruent" in clean_data.columns
    assert "responseCorrect", "responseTime" in clean_data.columns


def test_clean_other_data(other_data):
    # powinny zostać pominięte bez czyszczenia
    assert len(other_data) == len(sdh.clean_stroop_data(other_data))


def test_count_clean_stats(raw_data):
    clean_data = sdh.clean_stroop_data(raw_data)
    stats = sdh.count_stroop_stats(clean_data)
    assert len(stats) == 4  # tyle powinno być podgrup


def test_count_raw_stats(raw_data):
    # powinny zostać wyczyszczone przed liczeniem statystyk, więc wynik taki jak wyżej.
    assert len(sdh.count_stroop_stats(raw_data)) == 4


def test_count_other_stats(other_data):
    with pytest.raises(KeyError):
        sdh.count_stroop_stats(other_data)


def test_summarize(raw_data, other_data):
    summary = sdh.summarize_stroop(sdh.clean_stroop_data(raw_data))
    assert len(summary) == 1

    with pytest.raises(KeyError):
        sdh.summarize_stroop(other_data)
