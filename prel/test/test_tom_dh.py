import os
import pandas as pd
import pytest

from prel.data_handlers import tom_dh as tdh


@pytest.fixture()
def handler(): yield tdh.TomDataHandler(print_logs=True)


# tylko dane z treningu:
@pytest.fixture()
def raw_training_data(tmpdir) -> pd.DataFrame:
    tmpdir.join('temp').write_text(
        'adultMind,belief,desire,control,training.thisRepN,training.thisTrialN,training.thisN,training.thisIndex,food,cedrus.keyPressed,responseTime,responseCorrect,key_resp.keys,key_resp.corr,participant,gender,controller,date,expName,psychopyVersion,frameRate,\n,,,,,,,,,,,,,,JSPREL_15,f,c,2021_Nov_18_1840,preludiumTom,3.2.4,60.59284036388866,\n,,,,,,,,,,,,,,JSPREL_15,f,c,2021_Nov_18_1840,preludiumTom,3.2.4,60.59284036388866,\n,,,,,,,,,,,,,,JSPREL_15,f,c,2021_Nov_18_1840,preludiumTom,3.2.4,60.59284036388866,\n,,,,,,,,,,,,,,JSPREL_15,f,c,2021_Nov_18_1840,preludiumTom,3.2.4,60.59284036388866,\n,,,,,,,,,,,,,,JSPREL_15,f,c,2021_Nov_18_1840,preludiumTom,3.2.4,60.59284036388866,\n,,,,,,,,,,,,,,JSPREL_15,f,c,2021_Nov_18_1840,preludiumTom,3.2.4,60.59284036388866,\n,,,,,,,,,,,,,,JSPREL_15,f,c,2021_Nov_18_1840,preludiumTom,3.2.4,60.59284036388866,\n,,,,,,,,,,,,,,JSPREL_15,f,c,2021_Nov_18_1840,preludiumTom,3.2.4,60.59284036388866,\n,,,,,,,,,,,,,,JSPREL_15,f,c,2021_Nov_18_1840,preludiumTom,3.2.4,60.59284036388866,\n0,1,0,0,0,0,0,1,brokuły,1,0.25961500010453165,0,None,0,JSPREL_15,f,c,2021_Nov_18_1840,preludiumTom,3.2.4,60.59284036388866,\n1,0,1,1,0,1,1,5,banany,,,99,None,0,JSPREL_15,f,c,2021_Nov_18_1840,preludiumTom,3.2.4,60.59284036388866,\n0,1,0,1,0,2,2,4,orzeszki,1,1.4120779000222683,1,None,0,JSPREL_15,f,c,2021_Nov_18_1840,preludiumTom,3.2.4,60.59284036388866,\n1,0,0,0,0,3,3,3,gruszki,,,99,None,0,JSPREL_15,f,c,2021_Nov_18_1840,preludiumTom,3.2.4,60.59284036388866,\n0,1,1,0,0,4,4,0,wiśnie,,,99,None,0,JSPREL_15,f,c,2021_Nov_18_1840,preludiumTom,3.2.4,60.59284036388866,\n,,,,,,,,gruszki,,,,,,,,,,,,,\n',
        encoding='utf-8')
    data = pd.read_csv(os.path.dirname(__file__) + '/raw_tom_training_data.csv')
    yield data


@pytest.fixture()
def raw_data() -> pd.DataFrame:
    data = pd.read_csv(os.path.dirname(__file__) + '/raw_tom_data.csv')
    yield data


@pytest.fixture()
def other_data() -> pd.DataFrame:
    yield pd.DataFrame({'a': [1, None, 3], 'b': ['NA', 5, 6]})


def test_clean_training(raw_training_data):
    # po czyszczeniu danych z treningu nie powinno pozostac nic
    assert len(tdh.clean_tom_data(raw_training_data)) == 0


def test_clean_tom_data(raw_data):
    clean_data = tdh.clean_tom_data(raw_data)
    assert len(clean_data) == 192
    # istotnych danych z jednego eksperymentu ToM powinno być 192 linijek

    assert len(clean_data.columns == 6)
    # po czyszczeniu powinny zostać już tylko Ważne Kolumny + kolumna responseTime
    for name in tdh._IMPORTANT_CNAMES:
        assert name in clean_data.columns


def test_clean_other_data(other_data):
    # powinny zostać pominięte bez czyszczenia
    assert len(other_data) == len(tdh.clean_tom_data(other_data))


def test_count_clean_stats(raw_data):
    clean_data = tdh.clean_tom_data(raw_data)
    stats = tdh.count_tom_stats(clean_data)
    assert len(stats) == 8  # czyli 2^3, tyle jest podgrup
    assert 'responseTime_mean', 'noAns' in stats.columns


def test_count_raw_stats(raw_data):
    stats = tdh.count_tom_stats(raw_data)
    # powinny zostać wyczyszczone przed liczeniem statystyk, więc wynik taki jak wyżej.
    assert len(stats) == 8


def test_summarize_tom_data(raw_data):
    clean_data = tdh.clean_tom_data(raw_data)
    assert len(tdh.summarize_tom(clean_data)) == 1
