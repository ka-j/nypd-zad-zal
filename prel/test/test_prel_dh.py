import os

import pandas as pd
import pytest
from prel.data_handlers import prel_dh as pdh

pdh.PROJ_PREFIX = ''


@pytest.fixture()
def handler():
    yield pdh.PrelDataHandler(print_logs=True)


@pytest.fixture()
def prel_data():
    df = pd.DataFrame({'a': [1, None, 3], 'b': ['NA', 5, 6]})
    df.prel_id = 'test id'
    yield df


@pytest.mark.parametrize("test_input, expected", [
    ("stroop/temp/JSPREL_10_Stroop Effect.txt", '10'), ("1234", "1234"), ("abc.def", None), ("dir/otherdir123/abc999de666_2.py", '999')])
def test_get_id(test_input, expected):
    result = pdh.get_prel_id(test_input)
    assert result == expected


def test_merge_matching_data(prel_data):
    data1 = prel_data
    data2 = prel_data.copy()
    data2.prel_id = data1.prel_id
    result = pdh.merge_prel_data(data1, data2)
    assert result.prel_id == prel_data.prel_id
    assert "stroop_a", "ToM_b" in result.columns


def test_merge_different_ids(prel_data):
    data1 = prel_data
    data2 = prel_data.copy()
    data1.prel_id = 1
    data2.prel_id = 2
    with pytest.raises(AssertionError):
        pdh.merge_prel_data(data1, data2)


def test_merge_differenlen_data(prel_data):
    # dataframy różnych długości
    data1 = prel_data
    data2 = pd.concat([prel_data, pd.DataFrame({'a': [99], 'b': 42})])
    data2.prel_id = prel_data.prel_id
    assert len(data1) < len(data2)
    result = pdh.merge_prel_data(data1, data2)
    # to nie powinno być problemem dla pd.merge
    assert len(result) == len(data1) * len(data2)
    assert "participantId" in result.columns


def test_merge_empty_data():
    data1 = pd.DataFrame()
    data1.prel_id = 'test'
    data2 = pd.DataFrame()
    data2.prel_id = 'test'
    result = pdh.merge_prel_data(data1, data2)
    # wynikiem jest pusty df
    assert 0 == len(data1) == len(data2) == len(result)
    assert "participantId" in result.columns
    assert result.prel_id == 'test'


def test_merge_files(prel_data, tmpdir, handler):
    indir = tmpdir.mkdir('in')
    tomdir = indir.mkdir('tom')
    stroopdir = indir.mkdir('stroop')
    tomdir.join('abc123.csv').write('a,b,c\n1,2,3')
    tomdir.join('another123.csv').write('a,b,c\n1,2,3')
    tomdir.join('abc234.csv').write('a,b,c\n1,2,3')
    stroopdir.join('def123.txt').write('a,x,z\nNA,2,-1')
    stroopdir.join('def234.txt').write('a,x,z\nNA,2,-1')
    stroopdir.join('def567.txt').write('a,x,z\nNA,2,-1')
    result = handler.merge_prel_files(tomdir, stroopdir, tmpdir + '/out')
    assert len(result) == 2
    assert 'ToM_a' in result[0].columns
    assert '123_merged_stats.csv' in os.listdir(tmpdir + '/out')
    assert result[1].prel_id == '234'


def test_concat_data(handler, tmpdir):
    # tak naprawde testuje tylko pd.concat
    tmpdir.join('abc123.def').write('this is a file')
    tmpdir.join('abc234.def').write('this is a file')
    result = handler.concat_files(tmpdir)
    assert '123', '234' in result['participantId']


def test_verify_input(handler, tmpdir):
    tmpdir.join('abc123.def').write('this is a file')
    tmpdir.join('abc234.def').write('this is a file')
    tmpdir.join('hop.def').write('this is a file')  # ten plik powinien być pominięty bo nie ma w nazwie id
    tmpdir.join('def234.xyz').write('this is a file')  # powinien być pominięty bo ma złe rozszerzenie
    result = handler.verify_input_paths(tmpdir, file_extension='.def')
    assert len(result) == 2


def test_read_files(handler, tmpdir):
    tmpdir.join('abc123.def').write('this is a file')
    tmpdir.join('ghi123.jkl').write('c,o,l,,,\n1,2,,,\n,,,')
    result = handler.read_prel_files(tmpdir, exp_key='test')
    assert len(result) == 2
    assert len(result[1]) == 2 or len(result[0]) ==2    # nie wiemy w jakiej kolejności się wczytają
    assert result[0].exp_key == 'test'
    result2 = handler.read_prel_files(tmpdir, expected_len=4)
    # wynik powinien być ten sam, bo expected_len wpływa tylko na logi
    assert len(result2) == 2
    assert len(result2[1]) or len(result2[0]) == 2
    assert result2[0].prel_id == '123'


def test_read_files_multipleids(handler, tmpdir):
    tmpdir.join('abc123.def').write('this is a file')
    tmpdir.join('abc234.def').write('this is a file')
    tmpdir.join('hop.xd').write('this is a file')  # ten plik powinien być pominięty
    result = handler.read_prel_files(tmpdir, accept_multiple=False)
    assert len(result) == 2
    assert  '123', '234' in [x.prel_id for x in result]
