import os

import pandas as pd
import pytest
from prel.data_handlers import data_handler as dh


@pytest.fixture()
def handler():
    handler = dh.DataHandler(print_logs=True)
    yield handler


def test_handle_files(handler, tmpdir):
    indir = tmpdir.mkdir('in')
    indir.join('abc.csv').write('a,b,c\n1,2,3')

    # testuje output w postaci DataFrame:
    result = handler.handle_files(tmpdir + '/in')

    # output to jednoelementowa lista
    assert len(result) == 1
    # wczytany DataFrame powinien mieć długość 1
    assert len(result[0]) == 1
    assert 'b' in result[0].columns

    # testuje output wypisujący do plików:
    indir.join('ghi.csv').write('a,b,c\n1,2,3')

    result = handler.handle_files(tmpdir + '/in', tmpdir + '/out')

    assert len(os.listdir(tmpdir + '/out')) == 2
    assert len(result) == 2


def test_save_file(handler, tmpdir):
    # tak naprawde testujemy tylko pd.to_csv
    data = pd.DataFrame({"a": [1, 2], "b": [3, 4]})
    handler.save_file(data, tmpdir, 'abc.txt')
    assert 'abc.txt' in os.listdir(tmpdir)


@pytest.mark.parametrize("test_input, expected", [(('a,b,c'), 0), ('a,b,,c\n1,2,3', 1), ])
def test_read_file(handler, tmpdir, test_input, expected):
    tmpdir.join('abc.csv').write(test_input)
    assert len(handler.read_file(os.path.join(tmpdir, 'abc.csv'))) == expected


def test_read_nonexistent_file(handler):
    with pytest.raises(FileNotFoundError):
        result = handler.read_file('abc.csv')


def test_verify_existing_input(handler, tmpdir):
    fp = tmpdir.join('abc.txt')
    fp.write('this is a test file')
    result = handler.verify_input_paths(tmpdir)
    assert len(result) == 1
    assert len(handler.verify_input_paths(os.path.join(tmpdir, 'abc.txt'))) == 1
    assert handler.verify_input_paths(tmpdir) == [os.path.join(tmpdir, 'abc.txt')]

    tmpdir.join('def.json').write('another test file.')
    assert len(handler.verify_input_paths(tmpdir)) == 2

    tmpdir.join('def.json').write('a,b,c\n1,2,3')
    #  pliki json powinny być teraz pominięte
    assert len(handler.verify_input_paths(tmpdir, file_extension='.txt')) == 1


def test_verify_nonexistent_input(handler, tmp_path):
    with pytest.raises(FileNotFoundError):
        handler.verify_input_paths(os.path.join(tmp_path, "nonexistent"))


def test_verify_output(handler, tmpdir):
    # testuje istniejący folder:
    assert handler.verify_output(tmpdir) == os.path.abspath(tmpdir)
    # nieistniejący:
    assert isinstance(handler.verify_output(tmpdir + "/newdir"), str)
    assert os.path.exists(os.path.abspath(tmpdir + "/newdir"))
    # tworze plik i spróbuje go podać jako parametr:
    tmpdir.join('abc.d').write('to jest plik.')
    with pytest.raises(NotADirectoryError):
        handler.verify_output(tmpdir + "/abc.d")
