import argparse
import logging
import pandas as pd

from prel.data_handlers.prel_dh import PrelDataHandler

# niepotrzebne kolumny, tzn te które będą wyrzucone:

_COLUMNS_TO_DROP = ["Trial Number", "Stimulus Text", "Stimulus Color", "Response", "Block"]
_GROUP_CNAMES = ['Task', 'congruent']
_STATS_AGG = {'responseCorrect': 'mean', 'responseTime': ['mean', 'max', 'min', 'median']}


def magic(d): return {x: x.split('_')[1] for x in d if x.startswith('response')}


# oczekiwane rozszerzenie plików z danymi ze Stroopa
_FILE_EXTENSION = ".txt"


def clean_stroop_data(data: pd.DataFrame) -> pd.DataFrame:
    df = data.copy()

    try:
        # wyrzucamy niepotrzebne kolumny i dane z treningów
        df = df.loc[df['Block'] != "Practice"]
        df.drop(_COLUMNS_TO_DROP, axis=1, errors='ignore', inplace=True)
        # dopasowujemy nazwy kolumn do konwencji
        df.rename(columns={'RT': "responseTime", 'Response Accuracy': "responseCorrect", 'Condition': "congruent"}, inplace=True)
        # zmieniamy typ wartości w kolumnach na liczbowe, zwiększając czytelność
        df['responseCorrect'] = (df['responseCorrect'] == "rm_hit").astype(int)
        df['congruent'] = (df['congruent'] == "Neutral").astype(int)
        # normalizujemy responseTime do wspólnej jednostki: 1s
        df['responseTime'] = df['responseTime'] / 1000
        # resetujemy index:
        df.reset_index(drop=True, inplace=True)

        # sprawdzamy czy pozostały jeszcze jakieś NA w Ważnych Kolumnach (w kolumnie responseTime mogą być)
        for col in df.loc[:, df.columns != 'responseTime']:
            if df[col].isnull().any():
                logging.warning(f'napotkałem wartości NA w kolumnie {col}.')
    except KeyError as ke:
        logging.warning(f'w tym DataFrame: {df.columns} nie ma pewnych kolumn, których się spodziewałem: ({ke.args[0]})')
    if len(data) == len(df):
        logging.debug("wygląda na to że nie było nic do czyszczenia.")
    return df


def count_stroop_stats(data: pd.DataFrame) -> pd.DataFrame:
    if 'Stimulus Color' in data.columns:
        logging.info("znalazłem dane z prób treningowych w podanym df. Czyszczę zanim zacznę grupować dane.")
        data = clean_stroop_data(data)
    # grupujemy dane i liczymy statystyki dla każdej podgrupy
    grouped_trials = data.groupby(_GROUP_CNAMES, dropna=True).agg(_STATS_AGG)
    grouped_trials.columns = grouped_trials.columns.get_level_values(0) + '_' + grouped_trials.columns.get_level_values(1)
    grouped_trials.rename({'responseCorrect_mean': 'responseAccuracy'}, axis=1)
    grouped_trials.columns = grouped_trials.columns.to_flat_index()
    grouped_trials.reset_index(inplace=True)
    return grouped_trials


def summarize_stroop(data: pd.DataFrame) -> pd.DataFrame:
    df = data.copy()

    if 'responseTime_' not in str(df.columns):
        df = count_stroop_stats(df)
    df = df.loc[df.Task != 'Ink'].drop('Task', axis=1)  # dane z tego typu zadania nam się nie przydadzą (efekt Stroopa jest bardziej wyraźny w Color Name)
    ps = pd.DataFrame(df.agg(magic(df.columns))).T

    # liczymy efekt Stroopa
    df.sort_values(by=['congruent'])
    ps['stroopEffect'] = df['responseTime_mean'] - df['responseTime_mean'].shift(-1)
    ps.rename(columns={'responseCorrect_mean':'responseAccuracy'}, inplace=True)
    ps.reset_index(drop=True, inplace=True)
    return ps


class StroopDataHandler(PrelDataHandler):
    def __init__(self, input_data=None, print_logs: bool = False) -> None:
        super().__init__(input_data, file_extension=_FILE_EXTENSION, print_logs=print_logs)

    def handle_files(self, raw_input: str, output_dir: str = None, cleanser_func: callable = clean_stroop_data):
        return super().handle_files(raw_input, output_dir, cleanser_func)

    actions = {'clean': clean_stroop_data, 'stats': count_stroop_stats, 'summ': summarize_stroop}


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("input_dir")
    parser.add_argument("output_dir")
    parser.add_argument("action", nargs='?', default='clean')
    parser.add_argument("-v", action="store_true", default=False)
    args = parser.parse_args()

    dh = StroopDataHandler(print_logs=args.v)
    try:
        dh.handle_files(args.input_dir, args.output_dir, dh.actions[args.action])
    except KeyError:
        print(f'Niewłaściwy action. Do wyboru są: {list(dh.actions.keys())}')
