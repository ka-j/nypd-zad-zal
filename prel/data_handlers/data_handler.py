import csv
import logging
import os
import pandas as pd
from sys import stdout


class DataHandler():
    def __init__(self, input_data=None, file_extension: str = "", print_logs: bool = False) -> None:
        # ustawiamy logging:    todo: poprawić.
        if not logging.getLogger().hasHandlers():
            loghandler = logging.StreamHandler(stream=stdout)
            loghandler.setFormatter(logging.Formatter('%(asctime)s|%(module)s.%(funcName)s:%(lineno)d|%(levelname)s: %(message)s |', '%H:%M:%S'))
            logging.getLogger().addHandler(loghandler)
        if print_logs:
            logging.getLogger().setLevel(logging.INFO)
        else:
            logging.getLogger().setLevel(logging.WARNING)

        self.file_extension = file_extension

        if input_data is not None:
            self._data_frames = {'input': input_data}
        else:
            self._data_frames = {}
        self._done_ids = set()

    # region: funkcje do debuggingu
    def __getitem__(self, item):
        return self._data_frames[item]

    def __contains__(self, item):
        return item in self._data_frames

    def _keys(self):
        return self._data_frames.keys()

    # endregion

    def handle_files(self, raw_input: str, output_dir: str = None, cleanser_func: callable = (lambda x: x)) -> list:
        logging.debug(f'uruchamiam handle_files: raw_input={raw_input}, output_dir={output_dir}, cleanser_func={cleanser_func.__name__}')

        raw_paths = self.verify_input_paths(raw_input)

        if output_dir:
            output_dir = self.verify_output(output_dir)

        cleansed_dataframes = []
        done_files_counter = 0
        # iterujemy po plikach
        for path in raw_paths:
            filename = os.path.basename(path)
            try:
                # wczytuje dane:
                data = self.read_file(path)

                # pomijamy pliki bez danych:
                if len(data) == 0:
                    logging.info(f"{filename} to pusty plik, pomijam.")
                    continue

                # czyscimy za pomocą podanej funkcji
                data = cleanser_func.__call__(data)
                data = self._post_cleanse(data, filename)

                if len(data) == 0:
                    continue

                # jeśli coś w ogóle zostało po czyszczeniu, to zapisujemy:
                cleansed_dataframes.append(data)
                # do pliku:
                if output_dir:
                    self.save_file(data, output_dir, filename)
                    done_files_counter += 1

            except (ValueError, KeyError) as e:
                logging.error(f"coś poszło nie tak (prawdopodobnie zły format danych) przy pracy z plikiem {path} | Pomijam ten plik.")
            except Exception as e:
                logging.exception(e)
                logging.error(f"^coś poszło nie tak przy pracy z plikiem {path} | Pomijam ten plik.")


        logging.debug(f"skończyłem czyścić dane. Zwracam {len(cleansed_dataframes)} pd.DF |")
        if output_dir:
            logging.info(f" Stworzyłem {done_files_counter} plików w {output_dir}.")
            self._data_frames[os.path.relpath(output_dir)] = cleansed_dataframes
        else:
            self._data_frames['output'] = cleansed_dataframes

        return cleansed_dataframes

    def _post_cleanse(self, data:pd.DataFrame, filename:str) -> pd.DataFrame:
        return data


    def save_file(self, data: pd.DataFrame, output_dir: str, filename: str) -> None:
        # output powinien już być w tym momencie zweryfikowany
        if os.path.exists(os.path.join(output_dir, filename)):
            logging.warning(f"w folderze {os.path.dirname(output_dir)}/{os.path.basename(output_dir)} był już plik o tej samej nazwie: {filename} i zostanie nadpisany.")
        logging.debug(f"zapisałem {filename} w output_dir: {output_dir}")
        data.to_csv(os.path.join(output_dir, filename), index=False)

    def read_file(self, path: str) -> pd.DataFrame:
        # poniższe wydarzy się tylko jeśli ścieżka tego inputu nie była wcześniej zweryfikowana
        if not path.endswith(self.file_extension):
            logging.warning(f"nieoczekiwane rozszerzenie pliku: {path} | Spodziewałem się {self.file_extension}. Spróbuję odczytać plik mimo to.")

        logging.debug(f"wczytuję {os.path.dirname(path)}/{os.path.basename(path)}")
        with open(path, 'r') as fp:
            dialect = csv.Sniffer().sniff(fp.readline())
        df = pd.read_csv(path, dialect=dialect)
        return df

    def verify_input_paths(self, raw_input: str, file_extension: str = None) -> list:
        if not file_extension and self.file_extension:
            file_extension = self.file_extension
        logging.debug(f"weryfikuję ścieżkę: {raw_input}, file_extension={file_extension}")

        raw_input = os.path.abspath(raw_input)
        paths = []
        if not os.path.exists(raw_input):
            raise FileNotFoundError(f"podana ścieżka raw_input nie istnieje: {raw_input}")
        elif os.path.isfile(raw_input):
            if file_extension and not raw_input.endswith(file_extension):
                logging.warning(f"nieoczekiwane rozszerzenie pliku {raw_input} Spodziewałem się {file_extension} | Odrzucam ten plik.")
                return paths
            return [raw_input]
        elif os.path.isdir(raw_input):
            for filename in os.listdir(raw_input):
                if os.path.isdir(filename): continue
                if file_extension and not filename.endswith(file_extension):
                    logging.warning(f"nieoczekiwane rozszerzenie pliku w folderze raw_input: {filename} | Spodziewałem się {file_extension} | Odrzucam ten plik.")
                    continue
                paths.append(os.path.join(raw_input, filename))

            logging.info(f'w folderze {raw_input} jest {len(paths)} plików.')
        return paths

    def verify_output(self, output_dir):
        output_path = os.path.realpath(output_dir)
        if not os.path.exists(output_path):
            logging.info(f"podany output_dir nie istnieje więc zostanie utworzony na tej ścieżce: {output_path}.")
            os.makedirs(output_path)
        elif os.path.isdir(output_path):
            # sprawdzamy, jakie pliki byly juz obrobione
            logging.info(f"w output_dir jest już {len(os.listdir(output_dir))} plików.")
        else:
            raise NotADirectoryError(output_path)
        return output_path
