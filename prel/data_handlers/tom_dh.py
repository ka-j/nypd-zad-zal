import argparse
import logging
import pandas as pd

from prel.data_handlers.prel_dh import PrelDataHandler

# istotne kolumny:
_IMPORTANT_CNAMES = ["adultMind", "belief", "desire", "control", "responseCorrect"]

# niepotrzebne kolumny, tzn te które będą wyrzucone:
_COLUMNS_TO_DROP = ["gender", "participant", "food", "controller", "cedrus.keyPressed", "date", "expName", "psychopyVersion", "frameRate", '^Unnamed', "mouse_", "key_", "blocks",
                    "trials"]
_STATS_AGG = {'responseTime': ['mean', 'max', 'min', 'median']}

_FILE_EXTENSION = ".csv"


def magic(d):
    try:
        return {x: x.split('_')[1] for x in d if x.startswith('response')}
    except IndexError:
        return _STATS_AGG


def clean_tom_data(data: pd.DataFrame) -> pd.DataFrame:
    df: pd.DataFrame = data.copy()
    df.columns = df.columns.str.strip()
    try:
        # usuwamy nieistotne kolumny:
        for name in _COLUMNS_TO_DROP:
            df = df.loc[:, ~df.columns.str.contains(name)]

        # wyrzucamy dane z treningów:
        if 'training.thisN' in df.columns:
            df.drop(df[df['training.thisN'] >= 0].index, inplace=True, errors='ignore')
            df = df.loc[:, ~df.columns.str.startswith("training")]

        # wyrzucamy wiersze które składają się z samych wartości NA:
        df = df.loc[~df.isnull().all(1)]
        if len(df) > 0:
            # sprawdzamy czy pozostały jeszcze jakieś NA (w kolumnie responseTime i responseCorrect mogą być)
            for col in df.loc[:, df.columns != 'responseTime']:
                if df[col].isna().sum():
                    logging.warning(f'napotkałem wartości NA w kolumnie {col}.')
            # upewniamy się, że istotne kolumny mają dane w intach
            df[_IMPORTANT_CNAMES] = df[_IMPORTANT_CNAMES].astype(int)
        else:
            logging.debug("nic nie pozostało po czyszczeniu.")
    except KeyError as ke:
        logging.warning(f'w tym DataFrame: {str(df.head())} nie ma pewnych kolumn, których się spodziewałem: ({ke.args[0]})')

    # resetujemy index:
    df = df.reset_index(drop=True)

    if len(data) == len(df):
        logging.debug("wygląda na to że nie było nic do czyszczenia.")
    return df


def count_tom_stats(data: pd.DataFrame) -> pd.DataFrame:
    if 'training.thisN' in data.columns:
        logging.info("znalazłem dane z prób treningowych w podanym df. Czyszczę zanim zacznę grupować dane.")
        data = clean_tom_data(data)
        if len(data) == 0: return data  # nie ma z czego liczyć statystyk
    # opuszczamy dane z przypadków kontrolnych:
    trials = data[data['control'] == 0].drop('control', axis=1)

    # tych odpowiedzi powinno być 160

    # grupujemy dane żeby zliczyć poprawne/niepoprawne odpowiedzi
    group_cnames = ['adultMind', 'belief', 'desire']
    grouped_trials = trials.groupby(group_cnames + ['responseCorrect']).agg({'responseCorrect': 'count'}).unstack(fill_value=0)
    try:
        grouped_trials.columns = ['incorrectAns', 'correctAns', 'noAns']    # 0, 1, 99 - już posortowane
    except ValueError:  # w danych nie ma żadnych nieudzielonych odpowiedzi
        # więc ręcznie dodaję tę pustą kolumnę
        grouped_trials.columns = ['incorrectAns', 'correctAns']
        grouped_trials['noAns'] = 0

    # liczymy statystyki dla każdej podgrupy
    grouped_rtimes = trials.groupby(group_cnames, dropna=True).agg(_STATS_AGG)
    grouped_rtimes.columns = grouped_rtimes.columns.get_level_values(0) + '_' + grouped_rtimes.columns.get_level_values(1)

    joined = grouped_trials.join(grouped_rtimes)
    joined.columns = joined.columns.to_flat_index()
    joined.reset_index(inplace=True)
    return joined


def summarize_tom(data: pd.DataFrame) -> pd.DataFrame:
    df = data.copy()

    sums = df.sum()

    ps = pd.DataFrame(df.agg(magic(df.columns))).T
    try:
        ps['responseAccuracy'] = sums.correctAns / (sums.incorrectAns + sums.correctAns + sums.noAns)
    except AttributeError:
        # dane nie były wcześniej zgrupowane, więc liczymy inaczej:
        ps.columns = "responseTime_"+ps.columns
        ps['responseAccuracy'] = df['responseCorrect'].value_counts()[1]/len(df)
    ps.reset_index(drop=True)
    return ps


class TomDataHandler(PrelDataHandler):
    def __init__(self, input_data=None, print_logs: bool = False) -> None:
        super().__init__(input_data, file_extension=_FILE_EXTENSION, print_logs=print_logs)

    actions = {'clean': clean_tom_data, 'stats': count_tom_stats, 'summ': summarize_tom}


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("input_dir")
    parser.add_argument("output_dir")
    parser.add_argument("action", nargs='?', default='clean')
    parser.add_argument("-v", action="store_true", default=False)
    args = parser.parse_args()

    dh = TomDataHandler(print_logs=args.v)
    try:
        dh.handle_files(args.input_dir, args.output_dir, dh.actions[args.action])
    except KeyError:
        print(f'Niewłaściwy action. Do wyboru są: {list(dh.actions.keys())}')
