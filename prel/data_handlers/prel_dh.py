import argparse
import logging
import os
import re
from typing import Union

import pandas as pd
from prel.data_handlers.data_handler import DataHandler


PROJ_PREFIX = 'JSPREL'
MERGED_FILENAME = "merged_stats"

PRELID_PATTERN = re.compile(r'(\d+)')


# funkcja do zgadywania id na podstawie podanego tekstu (na ogół nazwy pliku)
def get_prel_id(file: str):
    search = PRELID_PATTERN.search(os.path.basename(file))
    if search:
        return search.group()
    else: return None


def merge_prel_data(tom_df, stroop_df) -> pd.DataFrame:
    # bierze dwie ramki i łączy je wzdłuż kolumn, dodaje kolumnę z id

    # zmieniam nazwy kolumn na tekst bo będą konkatenowane
    tom_df.columns = tom_df.columns.astype(str)
    stroop_df.columns = stroop_df.columns.astype(str)
    try:
        # przygotowujemy dataframy do mergu:
        assert stroop_df.prel_id == tom_df.prel_id
        tom_df.columns = 'ToM_' + tom_df.columns
        stroop_df.columns = 'Stroop_' + stroop_df.columns
        tom_df['participantId'] = tom_df.prel_id
        stroop_df['participantId'] = stroop_df.prel_id

    except AttributeError as e:
        logging.error("w co najmniej jednym z df brakuje atrybutu prel_id.")
        raise e
    except AssertionError as e:
        logging.error(f"podane df nie mają równych id. (tom_df.prel_id={tom_df.prel_id}, stroop_df.prel_id={stroop_df.prel_id})")
        raise e
    merged = pd.merge(tom_df, stroop_df, on='participantId')
    merged.prel_id = tom_df.prel_id
    return merged


class PrelDataHandler(DataHandler):
    def __init__(self, input_data=None, file_extension="", print_logs=False):
        super().__init__(input_data, file_extension, print_logs)

    def merge_prel_files(self, tom_input: str, stroop_input: str, output_dir: str = None) -> list:
        logging.debug(f'uruchamiam merge_prel_files: tom_input={tom_input}, stroop_input={stroop_input}, output_dir={output_dir}')

        if output_dir:
            output_dir = self.verify_output(output_dir)

        stroop_dfs = {x.prel_id: x for x in self.read_prel_files(stroop_input, 1, False, exp_key='Stroop')}
        stroop_ids = [x for x in stroop_dfs]

        merged_dfs = {}
        tom_ids = []
        for tom_df in self.read_prel_files(tom_input, 1, False, exp_key='ToM'):
            tom_ids.append(tom_df.prel_id)
            for other_id in stroop_ids:
                if other_id == tom_df.prel_id:
                    stroop_df = stroop_dfs.pop(other_id)
                    try:
                        merged = merge_prel_data(tom_df, stroop_df)
                        merged_dfs[other_id] = merged
                        if output_dir:
                            self.save_file(merged, output_dir, f"{PROJ_PREFIX}{other_id}_{MERGED_FILENAME}.csv")
                    except Exception:
                        logging.warning(f"Coś poszło nie tak przy mergowaniu plików dla id={other_id}. Pomijam.")
                    else:
                        logging.info(f"złączyłem pliki dla {tom_df.prel_id}.")

        dropped_counter = 0
        for tom_id in tom_ids:
            if tom_id not in merged_dfs:
                logging.warning(f"nie znalazłem danych ze Stroopa dla id={tom_id}.")
                dropped_counter += 1
        for stroop_id in stroop_dfs:
            logging.warning(f"nie znalazłem danych z ToMa dla id={stroop_id}.")
            dropped_counter += 1
        logging.info(f"skończyłem łączyć dane. Nie znalazłem pary dla {dropped_counter} plików.")
        if output_dir:
            logging.info(f"stworzyłem {len(merged_dfs)} plików.")
        else:
            logging.debug(f"zwracam {len(merged_dfs)} pd.DF")
        return list(merged_dfs.values())

    def concat_files(self, raw_input: str, output_filename: str = None) -> pd.DataFrame:
        logging.debug(f"uruchamiam concat_files: raw_input={raw_input}, output_dir={output_filename}")
        
        dfs = self.read_prel_files(raw_input, accept_multiple=False)
        for df in dfs:
            df['participantId'] = df.prel_id
        concat = pd.concat(dfs, ignore_index=True)
        if output_filename:
            self.save_file(concat, raw_input, output_filename)
        return concat

    def _post_cleanse(self, data: pd.DataFrame, filename:str) -> pd.DataFrame:
        data.prel_id = get_prel_id(filename)    # doda się po każdym użyciu handle_files
        return data

    def verify_input_paths(self, raw_input: str, file_extension: str = None) -> list:
        verified_paths = super().verify_input_paths(raw_input, file_extension)
        double_verified_paths = []
        ids = set()
        for path in verified_paths:
            # akceptujemy tylko nazwy plików które mają id:
            input_id = get_prel_id(os.path.basename(path))
            if input_id:
                double_verified_paths.append(path)
                if input_id in ids:
                    logging.info(f"znalazłem więcej niż jeden plik dla id={input_id}.")
                ids.add(input_id)
        return double_verified_paths

    def read_file(self, path: str) -> pd.DataFrame:
        # czyta plik + dodaje mu atrybut id
        df = super().read_file(path)
        df.prel_id = get_prel_id(path)
        return df

    


    def read_prel_files(self, input_path, expected_len: int = None, accept_multiple: bool = True, file_extension: str = None, exp_key: str = "") -> list:
        # wczyta wszystkie pliki z podanego folderu
        # exp_key to ciąg znaków (np Tom, Stroop) którym będą podpisane dane
        logging.debug(f"wczytuję {os.path.dirname(input_path)}")
        paths = self.verify_input_paths(input_path, file_extension)
        _dict_key_format = exp_key
        dfs = []
        read_ids = set()
        for path in paths:
            file_id = get_prel_id(os.path.basename(path))  # nie będzie None, bo ścieżka była zweryfikowana
            if exp_key != "":
                _dict_key_format = f'z {exp_key}a '
            if file_id in read_ids and not accept_multiple:
                logging.warning(f"więcej niż jeden plik z danymi {_dict_key_format}dla id={file_id}. Tylko ten pierwszy będzie wczytany.")
            else:
                df = self.read_file(path)
                if expected_len and len(df) != expected_len:
                    logging.warning(
                        f"spodziewałem się {expected_len} a znalazłem {len(df)} wierszy w pliku z danymi {_dict_key_format}: {os.path.basename(path)}. Możliwe nieoczekiwane rezultaty.")
                read_ids.add(file_id)
                df.prel_id = file_id
                if exp_key != "":
                    df.exp_key = exp_key
                dfs.append(df)
        logging.info(f"wczytałem {len(dfs)} plików")
        return dfs

    actions = {'merge': merge_prel_files, 'concat': concat_files}


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("input_dirs", nargs='+')
    parser.add_argument("output_dir")
    parser.add_argument("action")
    parser.add_argument("-v", action="store_true", default=False)
    args = parser.parse_args()

    dh = PrelDataHandler(print_logs=args.v)
    if args.action == 'merge':
        dh.merge_prel_files(args.input_dirs[0], args.input_dirs[1], args.output_dir)
    elif args.action == 'concat':
        dh.concat_files(args.input_dirs[0], args.output_dir)
    else:
        print(f'Niewłaściwy action. Do wyboru są: {list(dh.actions.keys())}')